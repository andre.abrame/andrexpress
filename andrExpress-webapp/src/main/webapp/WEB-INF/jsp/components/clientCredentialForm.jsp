<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

	<script src="js/clientCredentialFormValidation.js"></script>
	
				<fieldset>
					<legend>How can we identify you ?</legend>
					
					<label for="username">Username:</label>
					<input type="text" name="username" class="form-control" value="${editedUser.username}" />
					<div style="color:red; display:none"></div>
					
					<label for="password">Password:</label> 
					<input type="password" name="password" class="form-control" />
					<div style="color:red; display:none"></div>
					
					<label>Confirmation:</label>
					<input type="password" name="passwordConfirmation" class="form-control" />
					<div style="color:red; display:none"></div>
					
				</fieldset>