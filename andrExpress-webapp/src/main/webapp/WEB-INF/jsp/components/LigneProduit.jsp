<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<div class="ligneProduit" data-id="${ligneProduct.produit.id}">

	<div class="ImgProduit p-2">

		<c:if test="${ ligneProduct.produit.images.size() > 0 }">
			<img
				src="img/${ ligneProduct.produit.images.iterator().next().getAbsolutePath().substring(
					ligneProduct.produit.images.iterator().next().getAbsolutePath().lastIndexOf('/')+1) }"
				class="img-fluid">
		</c:if>
		<c:if test="${ ligneProduct.produit.images.size() == 0 }">
			<img src="img/spaghetti.jpg" class="img-fluid">
		</c:if>
	</div>

	<div class="Designation" >${ligneProduct.produit.nom}</div>

	<div class="PrixUnitaire">
		<p class="prixUnitaire"  data-id="${ligneProduct.produit.id}">${ligneProduct.produit.prix}</p>
	</div>

	<div class="QteProduit">
		<button class="btn btn-secondary m-2 ligneProduit-plus" data-id="${ligneProduct.produit.id}">
			<span class="fas fa-plus"></span>
		</button>
		<input type="text" class="quantite" name="number" size="5" value="${ligneProduct.nb}" data-id="${ligneProduct.produit.id}" data-previous-value="${ligneProduct.nb}">
		<button class="btn btn-secondary m-2 ligneProduit-moins" data-id="${ligneProduct.produit.id}">
			<span class="fas fa-minus"></span>
		</button>
	</div>

	<div class="TotalLigne">
		<p class="prixTotal" data-id="${ligneProduct.produit.id}">${ligneProduct.total()}</p>
	</div>

	<div class="p-2">
		<button class="btn btn-secondary btn-block ligneProduit-supprime" data-id="${ligneProduct.produit.id}">
			<span class="fas fa-trash-alt"></span>
		</button>
	</div>

</div>