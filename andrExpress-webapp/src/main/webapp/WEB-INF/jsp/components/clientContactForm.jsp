<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


	<script src="js/clientContactFormValidation.js"></script>

				<fieldset>
					<legend>How can we contact you ?</legend>
					<label for="email">Email :</label>
					<input type="text" name="email" placeholder="dummy@example.com" class="form-control" value="${editedUser.email}" />
					<div style="color:red; display:none"></div>
					<label for="tel">Phone:</label>
					<input type="tel" name="tel" placeholder="0102030405" class="form-control"/>
					<label for="adress">Addresses:</label>
					<div id="addresses-container">
					
						<c:forEach var="adresse" items="${editedUser.adresses}">
							<div class="form-inline mb-2">
								<input type="text" style="display:none;" name="id" value="${adresse.id}">
								<input type="text" style="width:55%; margin-right:1%;" placeholder="123 chemin des oiseaux" class="form-control" name="rue" value="${adresse.rue}"/>
								<input type="text" style="width:30%; margin-right:1%;" placeholder="Marseille" class="form-control" name="ville" value="${adresse.ville}"/>
								<input type="text" style="width:10%;" placeholder="13001" class="form-control" name="codepostal" value="${adresse.codePostal}"/>
								<i class="fas fa-trash-alt p-2" style="visibility:visible"></i>
							</div>
						</c:forEach>
					
					
						<div class="form-inline">
							<input type="text" style="display:none;" data-name="id">
							<input type="text" style="width:55%; margin-right:1%;" placeholder="123 chemin des oiseaux" class="form-control" data-name="rue"/>
							<input type="text" style="width:30%; margin-right:1%;" placeholder="Marseille" class="form-control" data-name="ville"/>
							<input type="text" style="width:10%;" placeholder="13001" class="form-control" data-name="codepostal"/>
							<i class="fas fa-trash-alt p-2" style="visibility:hidden"></i>
						</div>
						
					</div>
				</fieldset>
