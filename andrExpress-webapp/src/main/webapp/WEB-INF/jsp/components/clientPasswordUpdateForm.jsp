<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
				
				<fieldset>
					<legend>Update your password</legend>
					<label for="password">Old password:</label> 
					<input type="password" name="oldPassword" class="form-control" />
					<label for="password">New password:</label> 
					<input type="password" name="newPassword" class="form-control" />
					<label>Confirmation:</label>
					<input type="password" name="newPasswordConfirmation" class="form-control" />
				</fieldset>