<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<fieldset>
	<legend>Edit product</legend>
	<input name="id"  value="${produit.id}" style="display: none;"></input>
	<label for="name">Name:</label> <input type="text" name="name"
		class="form-control" value="${produit.nom}" /> <label for="description">Description:</label>
	<textarea name="description" class="form-control"value="${produit.description}"></textarea>
	<label for="price">Price:</label> <input type="text" name="price"
		class="form-control" value="${produit.prix}"> <label for="images">Images:</label>

	<div class="border rounded"
		style="width: 100%; display: grid; grid-template-columns: 1fr 1fr;">
		<div class="text-center border-right border-bottom p-1">File
			from URL</div>
		<div class="text-center border-bottom p-1">File from Disk</div>
		<div class="border-right p-1" style="height: 100px;">
			<ul id="file-from-url-container" class="list-unstyled m-1">
			</ul>
		</div>
		<div class="p-1" style="height: 100px;">
			<ul id="file-from-disk-container" class="list-unstyled m-1">
			</ul>
		</div>
		<div style="display: grid; grid-template-columns: 1fr auto auto;"
			class="p-1 border-right">
			<input id="file-from-url-input" type="text" name="fileUrl"
				class="form-control ml-1">
			<button class="btn btn-success ml-2" type="button" disabled>
				<span class="fas fa-plus fa-lg"></span>
			</button>
			<button class="btn btn-danger ml-1" type="button" disabled>
				<span class="fas fa-times fa-lg"></span>
			</button>
		</div>
		<div style="display: grid; grid-template-columns: 1fr auto auto;"
			class="p-1">
			<input id="file-from-disk-input" type="file" name="fileDisk"
				class="form-control-file ml-1">
			<button class="btn btn-success ml-1" type="button" disabled>
				<span class="fas fa-plus fa-lg"></span>
			</button>
			<button class="btn btn-danger ml-1" type="button" disabled>
				<span class="fas fa-times fa-lg"></span>
			</button>
		</div>
	</div>
</fieldset>

