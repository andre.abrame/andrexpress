<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<div class="comments">

	<c:forEach items="${comments}" var="comment">
	
		<div class="Titre">
			<h5>${comment.titre}</h5>
			<div class="rating rating-value-${comment.note}"></div>
			<div class="Auteur">
				<h6>${comment.client.prenom} ${comment.client.nom}</h6>
				<span>${comment.date}</span>
				
				<div class="Texte">
					<h6>${comment.texte}</h6>
				</div>
			</div>
		</div>
	</c:forEach>
</div>