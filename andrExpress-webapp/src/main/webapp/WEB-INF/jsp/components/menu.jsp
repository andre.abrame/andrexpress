<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="menu bg-dark py-2">
	<div class="menu-item-left">
	</div>
	<div class="menu-item-center">
		<form class="form-row align-items-center" action="search" method="GET">
			<input class="form-control col mr-sm-2" type="search"
				placeholder="Search" aria-label="Search" name="searched">
			<button class="btn btn-outline-light col-auto my-2 my-sm-0 mr-sm-2"
				type="submit">Search</button>
			<button class="btn btn-outline-light col-auto my-2 my-sm-0"
				type="button">Filters</button>
		</form>
	</div>
	<div class="menu-item-right">
		<c:if test="${!empty user}">
			<c:if test="${user.hasRole('Client')}">
				<a href=panier class="btn btn-outline-light">
					<span class="fas fa-shopping-basket"></span>
					<span id="panierTotalNb"></span>
				</a>
			</c:if>
			<div class="dropdown" style="display:inline-block;">
				<button class="btn btn-outline-light dropdown-toggle" type="button"
					id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
					aria-expanded="false">
					<span class="fas fa-user mr-1"></span> Bonjour ${user.prenom}
				</button>
				<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
					<c:if test="${user.hasRole('Admin')}">
						<a href=product/new class="btn btn-outline-light dropdown-item">
							<span class="fas fa-plus mr-1"></span> Add product
						</a>
					</c:if>
					<a href="client/edit" class="btn btn-outline-light dropdown-item">
						<span class="fas fa-user-edit mr-1"></span> Edit user
					</a> <a href=logout class="btn btn-outline-light dropdown-item"> <span
						class="fas fa-sign-out-alt mr-1"></span> Sign out
					</a>
				</div>
			</div>
		</c:if>
		<c:if test="${empty user}">
			<a href="client/new" class="btn btn-outline-light"> <span
				class="fas fa-user-plus mr-1"></span> Sign up
			</a>
			<a href="login" class="btn btn-outline-light"> <span
				class="fas fa-sign-in-alt mr-1"></span> Sign in
			</a>
		</c:if>
	</div>
</div>