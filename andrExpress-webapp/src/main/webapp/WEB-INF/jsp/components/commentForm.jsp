<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

    
    <div class="container">
		<form action="product/comments" method="POST">
			<div class="form-group">
				<label for="comment">Commentaire :</label>

				<input type="text" class="form-control" id="usr" name="titre" placeholder="Titre de votre commentaire.." style="margin-bottom: 1%;" value="${editedComment.titre}">
				<textarea class="form-control" rows="5" id="comment" name="texte" placeholder="Votre commentaire..."  value="${editedComment.texte}"></textarea>
				<input type="number" class="form-control" id="note" name="note" placeholder="Note de votre commentaire.." style="margin-top: 1%;" value="${editedComment.note}" min="0" max="5">
				<input type="number" name="id" class="form-control" style="display: none;" value="${product.id}">
			</div>
			<div>
				<button type="submit" class="btn btn-primary" style="float: right;"  >Envoyer</button>
			</div>
		</form>
	</div>