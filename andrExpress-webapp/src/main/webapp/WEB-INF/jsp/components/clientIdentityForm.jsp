<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

	<script src="js/clientIdentityFormValidation.js"></script>
	
				<fieldset>
					<legend>Who are you ?</legend>
					<label for="prenom">Firstname: </label> 
					<input type="text" id="prenom" name="prenom" class="form-control" value="${editedUser.prenom}" />
					<div style="color:red; display:none"></div>
					<label for="nom">Lastname:</label>
					<input type="text" name="nom" class="form-control" value="${editedUser.nom}" />
					<div style="color:red; display:none"></div>
				</fieldset>