<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>AndrExpress - Panier</title>



<!-- JQuery -->
	<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!-- Popper -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

	<!-- FontAwesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

	<!-- Custom CSS & JS -->
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" type="text/css" href="css/panier.css">
	<link rel="stylesheet" type="text/css" href="css/ligneProduit.css">
	<link rel="stylesheet" type="text/css" href="css/forms.css">
	<script src="js/produits-grid.js"></script>
	<script src="js/images-selector.js"></script>
	<script src="js/ligneProduit.js"></script>
	<script src="js/panier.js"></script>
	<script>var ctx = "${pageContext.request.contextPath}"</script>
	
</head>
<body>

		<!-- Header ---------------------------------------------------------->
		<%@ include file="components/header.jsp" %>


		<!-- Menu ------------------------------------------------------------>
		<%@ include file="components/menu.jsp" %>


		<!-- Content --------------------------------------------------------->
		<div class="content">
			<div class="panier">
				<div class="titreLigneProduit">
					<div>
						<h6>Image</h6>
					</div>
					<div>
						<h6>Désignation</h6>
					</div>
					<div>
						<h6>
							Prix unitaire
						</h6>
					</div>
					<div>
						<h6>Quantité</h6>
					</div>
					<div>
						<h6>Total</h6>
					</div>
				</div>
		
				<c:forEach items="${user.panier.produits}" var="ligneProduct">
					<%@ include file="components/LigneProduit.jsp" %>
				</c:forEach>
			</div>
		
		
		<!-- AFFICHER PRIXTOTAL  -->
		<div class="prixTotalPanier"> 
			<h3>Prix Total : <i class="prixTotalPanierValue">${user.panier.total()}</i></h3>
		</div>
		
		
		<!--  BOUTON-->
		<div class="text-center">
			<a href="." class="btn btn-secondary" >Continuer Mes Achats</a>
			<a class="btn btn-success">Commander</a>
		</div>
	
		</div>
		<!-- Footer ---------------------------------------------------------->
		<%@ include file="components/footer.jsp" %>
		
</body>
</html>
