<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>AndrExpress</title>

<!-- base url for all links --><c:set var="url">${pageContext.request.requestURL}</c:set>
	<base
			href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />


<!-- JQuery -->
<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.0.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Popper -->
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
	integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
	crossorigin="anonymous"></script>

<!-- Bootstrap -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
	integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
	crossorigin="anonymous"></script>

<!-- FontAwesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.7.0/css/all.css"
	integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ"
	crossorigin="anonymous">

<!-- Custom CSS & JS -->
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" type="text/css" href="css/products.css">
<link rel="stylesheet" type="text/css" href="css/product.css">
<script src="js/produits-grid.js"></script>
<script src="js/search.js"></script>
<script src="js/rating.js"></script>

</head>
<body>



	<!-- Header ---------------------------------------------------------->
	<%@ include file="components/header.jsp"%>


	<!-- Menu ------------------------------------------------------------>
	<%@ include file="components/menu.jsp"%>


		<!-- Content --------------------------------------------------------->
	<div class="content">

		<fieldset>
			<legend>${product.nom}</legend>
			<div class="fiche-produit">

				


					<div class="image-fiche">
						<div class="image">
						<span class="helper"></span>
						<c:if test="${ product.images.size() > 0 }">
							<img
								src="img/${product.images.iterator().next().getAbsolutePath().substring(product.images.iterator().next().getAbsolutePath().lastIndexOf('/')+1)}"
								class="img-fluid">
						</c:if>
						<c:if test="${ product.images.size() == 0 }">
							<img src="img/spaghetti.jpg" class="img-fluid">
						</c:if>
					</div>
					</div>

					<div class="text-fiche">
						<div class="name">
							<h1>${product.nom}</h1>
						</div>
						<div class="description">${product.description}</div>
						<div class="rating rating-value-4.5"></div>
					</div>

					<div class="commande-fiche">
						<div class="price-fiche">${product.prix}</div>
						<div>
							<i class="fa fa-l fa-plus-circle" aria-hidden="true"
								data-id="${product.id}"></i> <input type="text" class="quantite"
								name="number" value="0" data-id="${product.id}"> <i
								class="fa fa-minus-circle quantite" aria-hidden="true"
								data-id="${product.id}"></i>
						</div>
						<div class="ajout-panier">
							<a href="" class="btn btn-outline-light dropdown-item">
								<span class="fas fa-credit-card mr-1"></span> Commander
							</a>
						</div>
						<div class="suppression-produit">
							<a href="" class="btn btn-outline-light dropdown-item">
								<span class="fas fa-trash mr-1"></span> Supprimer produit
							</a>
						</div>
						<div class="edit-produit">
							<a href="" class="btn btn-outline-light dropdown-item">
								<span class="fas fa-sign-out-alt mr-1"></span> Edit produit
							</a>
						</div>
					</div>




			</div>
			
			
		<div Class="ligneProduct">

			<c:set scope="request" var="comments" value="${ product.commentaires }"></c:set>
			<%@ include file="components/comments.jsp"%>
			
		</div>
		
		<div>
		
		<%@ include file="components/commentForm.jsp"%>
		
		</div>
			
		</fieldset>
	</div>

</body>
</html>
