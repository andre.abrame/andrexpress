
$(document).ready(function(){
	
	function lastLineKeyUpHandler() {
		// on récupere l'ancienne derniere ligne
		var oldl = $("#addresses-container").children().last();
		// on creer un nouvelle derniere ligne
		$("#addresses-container:last-child").append(
				'<div class="form-inline">\
					<input type="text" style="display:none;" data-name="id">\
					<input type="text" style="width:55%; margin-right:1%;" placeholder="Numéro et rue" class="form-control" data-name="rue"/>\
					<input type="text" style="width:30%; margin-right:1%;" placeholder="Ville" class="form-control" data-name="ville"/>\
					<input type="text" style="width:10%;" placeholder="Code postal" class="form-control" data-name="codepostal"/>\
					<i class="fas fa-trash-alt p-2" style="visibility:hidden"></i>\
				</div>');
		// on mémorise la nouvelle deniere ligne
		var newl = $("#addresses-container").children().last();
		// on detache les gestionnaires d'evenements des inputs de l'ancienne derniere ligne
		oldl.children("input").off("keyup", lastLineKeyUpHandler);
		// on attache les gestionnaires d'evenements aux inputs de la nouvelle derniere ligne
		newl.children().on("keyup", lastLineKeyUpHandler);
		// on affiche le bouton remove de l'ancienne derniere ligne
		oldl.children("i").css("visibility", "visible")
		// on ajoute les attributs name à la ligne courante
		oldl.children("input").each(function() {
			$(this).attr("name", $(this).attr("data-name"));
		});
		// on attache un gestionnaire d'evenement pour le clique sur la poubelle
		newl.children("i").click(function() {
			$(this).parent().remove();
		});
		// on ajoute une marge bas à l'ancienne derniere ligne
		oldl.addClass("mb-2");
		// on attache un gestionnaire d'evenements sur l'avant derniere ligne
		oldl.children("input").on("focusout", function() {
			// si tous les inputs sont vides
			$(this).css("color", "red");
			oldl.children("input").filter(function() { return (($.trim($(this).val()).length != 0) || $(this).is(":focus")); }).css("border", "5px solid green");
			if (oldl.children("input").filter(function() { return (($.trim($(this).val()).length != 0) || $(this).is(":focus")); }).length == 0) {
				// on supprime la ligne
				oldl.remove();
			}
		});
	}
	
	$("#addresses-container").children().last().children().on("keyup", lastLineKeyUpHandler);
	
	$("#addresses-container").find("i").click(function() {
		$(this).parent().remove();
	});

});