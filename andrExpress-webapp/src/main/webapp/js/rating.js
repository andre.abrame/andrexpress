
//funtion updating rating displays according to the rating-value-?? classes
function updateRatingDisplayFromClass() {
	var classes = $(this).attr("class").split(" ");
	var value = 5;
	for (var c of classes) {
		if (c.search("rating-value-") == 0) {
			value = Number(c.substring(13));
			break;
		}
	}
	$(this).nextAll("input").val(value);
	updateRatingDisplay($(this), value);
}

// function updating the display according to its arguments
function updateRatingDisplay(rating, value) {
	rating.find("span.fa-star").each(function() {
		if ($(this).attr("data-rating") <= value) {
			$(this).css("color", "orange");
		} else if ($(this).attr("data-rating") < value+1) {
			var dif = value+1 - Number($(this).attr("data-rating"));
			$(this).css({"background": "-webkit-gradient(linear, left top, right top, from(orange), color-stop("+dif+", orange), color-stop("+dif+", black), to(black))",
				"-webkit-background-clip": "text",
				"-webkit-text-fill-color": "transparent"});
		} else {
			$(this).css("color", "black");
		}
	});
}



function activateRating() {


	// adding stars to rating elements
	$(".rating, .rating-input").append('<span>\
			<span class="fa fa-star" data-rating="1"></span> \
			<span class="fa fa-star" data-rating="2"></span> \
			<span class="fa fa-star" data-rating="3"></span> \
			<span class="fa fa-star" data-rating="4"></span> \
			<span class="fa fa-star" data-rating="5"></span> \
			<input type="text" value="5" style="display: none;">\
	</span>');

	// changing cursor on rating-input element
	$(".rating-input").hover(function() {
		$(this).css("cursor", "pointer");
	});

	// updating initial rating display
	$(".rating, .rating-input").each(updateRatingDisplayFromClass); 


	// changing value when clicking on rating-input element
	$(".rating-input > span > .fa-star").click(function() {
		var rating = $(this).attr("data-rating");
		var input = $(this).nextAll("input");
		input.val(rating);
		// updating display
		updateRatingDisplay($(this).parents(".rating, .rating-input"), rating);
	});
};



jQuery.fn.activateRating = function() {
	// adding stars to rating elements
	$(this).find(".rating, .rating-input").append('<span>\
			<span class="fa fa-star" data-rating="1"></span> \
			<span class="fa fa-star" data-rating="2"></span> \
			<span class="fa fa-star" data-rating="3"></span> \
			<span class="fa fa-star" data-rating="4"></span> \
			<span class="fa fa-star" data-rating="5"></span> \
			<input type="text" value="5" style="display: none;">\
	</span>');

	// changing cursor on rating-input element
	$(this).find(".rating-input").hover(function() {
		$(this).css("cursor", "pointer");
	});

	// updating initial rating display
	$(this).find(".rating, .rating-input").each(updateRatingDisplayFromClass); 


	// changing value when clicking on rating-input element
	$(this).find(".rating-input > span > .fa-star").click(function() {
		var rating = $(this).attr("data-rating");
		var input = $(this).nextAll("input");
		input.val(rating);
		// updating display
		updateRatingDisplay($(this).parents(".rating, .rating-input"), rating);
	});
	return this;
};




$(document).ready(function(){

	$(document.body).activateRating();




});