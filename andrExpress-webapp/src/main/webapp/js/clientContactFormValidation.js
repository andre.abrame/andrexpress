
$(document).ready(function(){
	
	var error;
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	function invalid(e, m) {
		e.css("border", "2px solid red");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}
	
	function valid(e) {
		e.next().hide();
		e.css("border", "2px none red");
	}
	
	
	
	
	
	$(".content form").submit(function() {
		error = false;
		
		// email validation 
		// email != ""
		if ($("input[name=email]").val() == "") {
			invalid($("input[name=email]"), "The email is mandatory.");
		}
		// email format
		else if (!emailRegExp.test($("input[name=email]").val())) {
			invalid($("input[name=email]"), "The email format is invalid.");
		} else {
			valid($("input[name=email]"));
		}
		
		return !error;
	});
	
});