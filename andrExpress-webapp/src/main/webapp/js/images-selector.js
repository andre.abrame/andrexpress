
$(document).ready(function(){
	
	
	function fileFromUrlAddClickHandler() {
		// recuperation de la valeur du input
		var fileFromUrl = $("#file-from-url-input").val();
		// recuperation du conteneur
		var container = $("#file-from-url-container");
		// ajout d'un ligne au conteneur
		container.append(
			'<li class=""> \
				<button type="button" class="btn btn-danger btn-sm rounded-circle mr-1" style="font-size:6px;padding: 2px 4px">\
					<span class="fas fa-times fa-lg" ></span>\
				</button>' +
				fileFromUrl +
				'<input type="text" style="display:none" name="filesFromUrl" value="'+fileFromUrl+'">\
			</li>'
		);
		// ajout d'un listener sur le boutton supprimer de la nouvelle ligne
		container.children().last().children("button").click(fileRemoveClickHandler);
		// reinitialisation du input
		$("#file-from-url-input").val("");
		$("#file-from-url-input").next().attr("disabled", "disabled");
		$("#file-from-url-input").next().next().attr("disabled", "disabled");
	}

	function fileFromDiskAddClickHandler() {
		// recuperation du input, de la valeur du input et du conteneur
		var oldInput = $(this).prev();
		var fileFromDisk = $(this).prev().val();
		var container = $("#file-from-disk-container");
		// ajout d'un ligne au conteneur
		container.append(
			'<li class=""> \
				<button type="button" class="btn btn-danger btn-sm rounded-circle mr-1" style="font-size:6px;padding: 2px 4px">\
					<span class="fas fa-times fa-lg" ></span>\
				</button>' +
				fileFromDisk +
			'</li>'
		);
		// ajout d'un listener sur le bouton supprimer de la nouvelle ligne
		container.children().last().children("button").click(fileRemoveClickHandler);
		// ajout d'un nouvel input
		var newInput = oldInput.clone();
		oldInput.after(newInput);
		// mide à jour de l'ancien input
		oldInput.hide();
		oldInput.attr("name", "filesFromDisk");
		container.append(oldInput);
		// reinitialisation du input
		newInput.val("");
		newInput.next().attr("disabled", "disabled");
		newInput.next().next().attr("disabled", "disabled");
		newInput.change(fileInputChangeHandler);
	}
	

	function fileRemoveClickHandler() {
		$(this).parent().remove();
	}
	
	function fileResetClickHandler() {
		// reinitialisation du input
		$(this).prev().prev().val("");
		$(this).prev().attr("disabled", "disabled");
		$(this).attr("disabled", "disabled");
		
	}
	
	function fileInputChangeHandler() {
		// mise à jour des boutons
		if ($(this).val() == "") {
			$(this).next().attr("disabled", "disabled");
			$(this).next().next().attr("disabled", "disabled");
		} else {
			$(this).next().removeAttr("disabled");
			$(this).next().next().removeAttr("disabled");
		}
	}
	
	
	$("#file-from-url-input").next().click(fileFromUrlAddClickHandler);
	$("#file-from-url-input").next().next().click(fileResetClickHandler);
	$("#file-from-url-input").on("input", fileInputChangeHandler);

	$("#file-from-disk-input").next().click(fileFromDiskAddClickHandler);
	$("#file-from-disk-input").next().next().click(fileResetClickHandler);
	$("#file-from-disk-input").change(fileInputChangeHandler);
	

});