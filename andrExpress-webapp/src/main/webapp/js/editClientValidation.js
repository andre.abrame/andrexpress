
$(document).ready(function(){
	
	var error;
	var emailRegExp = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	
	function invalid(e, m) {
		e.css("border", "2px solid red");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}
	
	function valid(e) {
		e.next().hide();
		e.css("border", "2px none red");
	}
	
	
	
	
	
	$(".content form").submit(function() {
		error = false;
		// username validation
		if ($("input[name=username]").val() == "") {
			invalid($("input[name=username]"), "The username is mandatory.");
		} else {
			valid($("input[name=username]"));
		}
		
		// password validation
		// password != ""
		if ($("input[name=password]").val() == "") {
			invalid($("input[name=password]"), "The password cannot be empty.");
		}
		// password length < 8
		else if ($("input[name=password]").val().length > 8) {
			invalid($("input[name=password]"), "The password must be at most 8 characters long.");
		}
		// password == confirmation
		else if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) { // 
			invalid($("input[name=password]"));
		} else {
			valid($("input[name=password]"));
		}
		
		// passwordConfirmation validation
		// password  == confirmation
		if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) {
			invalid($("input[name=passwordConfirmation]"), "The confirmation does not match the password.");
		} else {
			valid($("input[name=passwordConfirmation]"));
		}
		
		// email validation 
		// email != ""
		if ($("input[name=email]").val() == "") {
			invalid($("input[name=email]"), "The email is mandatory.");
		}
		// email format
		else if (!emailRegExp.test($("input[name=email]").val())) {
			invalid($("input[name=email]"), "The email format is invalid.");
		} else {
			valid($("input[name=email]"));
		}
		
		// firstname validation
		// firstname != ""
		if ($("input[name=prenom]").val() == "") {
			invalid($("input[name=prenom]"), "The firstname is mandatory.");
		} else {
			valid($("input[name=prenom]"));
		}
		
		// lastname validation
		// lastname != ""
		if ($("input[name=nom]").val() == "") {
			invalid($("input[name=nom]"), "The lastname is mandatory.");
		} else {
			valid($("input[name=nom]"));
		}
		return !error;
	});
	
});