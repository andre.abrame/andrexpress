
// function sending an ajax POST request for updating the user panier 
function updatePanier(productId, oQ, nQ) {
	$.ajax({
		method: "POST",
		url: "panier",
		data: {product: productId,quantity: nQ }
	})
	.done(function() {
		$("input.quantite[data-id="+productId+"]").val(nQ);
		updateProductButtonsVisibility($(".product[data-id="+productId+"]"));
	});
};


// function updating the visibility of the panier quantity
function updateProductButtonsVisibility(product) {
	if (product.find("input.quantite").val() == "0") {
		product.find(".ajout-panier").show();
		product.find(".panier-controle").hide();
	} else {
		product.find(".ajout-panier").hide();
		product.find(".panier-controle").show();
	}
}


// function attaching the events listener to the panier control elements and updating visibility
function attachEventListenersToProducts() {

	// plus button
	$("button.produit-plus").click(function() {
		console.log("plus");
		let oQ = Number($(this).next().val());
		let nQ = oQ+1;
		let id = $(this).attr("data-id");
		updatePanier(id, oQ, nQ);
	});

	// minus button
	$("button.produit-moins").click(function() {
		console.log("moins");
		let oQ = Number($(this).prev().val());
		let nQ = oQ-1;
		let id = $(this).attr("data-id");
		updatePanier(id, oQ, nQ);
	});

	// updating visibility
	$(".product").each(function() {
		updateProductButtonsVisibility($(this));
	});

}



jQuery.fn.visible = function() {
	return this.css('visibility', 'visible');
};

jQuery.fn.invisible = function() {
	return this.css('visibility', 'hidden');
};

jQuery.fn.visibilityToggle = function() {
	return this.css('visibility', function(i, visibility) {
		return (visibility == 'visible') ? 'hidden' : 'visible';
	});
};






$(document).ready(function(){

	attachEventListenersToProducts();




}); 