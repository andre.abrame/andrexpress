
$(document).ready(function(){
	
	var error;
	
	function invalid(e, m) {
		e.css("border", "2px solid red");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}
	
	function valid(e) {
		e.next().hide();
		e.css("border", "2px none red");
	}
	
	
	
	
	$("input[name=username]").keyup(function() {
		// envoyer une requete
		var req = new XMLHttpRequest();
		req.open("GET", "client/exists?username="+$(this).val(), true);
		req.onreadystatechange = function() {
		    if (this.readyState == 4 && this.status == 200) {
		    	if (this.responseText == "true") {
		    		invalid($("input[name=username]"), "Username already in use.");
		    	} else {
		    		valid($("input[name=username]"));
		    	}
		    }
		};
		req.send();
	});
	
	
	
	
	$(".content form").submit(function() {
		error = false;

		// username validation
		if ($("input[name=username]").val() == "") {
			invalid($("input[name=username]"), "The username is mandatory.");
		} else {
			valid($("input[name=username]"));
		}
		
		// password validation
		// password != ""
		if ($("input[name=password]").val() == "") {
			invalid($("input[name=password]"), "The password cannot be empty.");
		}
		// password length < 8
		else if ($("input[name=password]").val().length > 8) {
			invalid($("input[name=password]"), "The password must be at most 8 characters long.");
		}
		// password == confirmation
		else if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) { // 
			invalid($("input[name=password]"));
		} else {
			valid($("input[name=password]"));
		}
		
		// passwordConfirmation validation
		// password  == confirmation
		if ($("input[name=passwordConfirmation]").val() != $("input[name=password]").val()) {
			invalid($("input[name=passwordConfirmation]"), "The confirmation does not match the password.");
		} else {
			valid($("input[name=passwordConfirmation]"));
		}
	
		return !error;
	});
	
});