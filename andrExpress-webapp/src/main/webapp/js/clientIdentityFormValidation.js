
$(document).ready(function(){
	
	var error;
	
	function invalid(e, m) {
		e.css("border", "2px solid red");
		if (m !== undefined) {
			e.next().text(m);
			e.next().show();
		} else {
			e.next().hide();
		}
		error = true;
	}
	
	function valid(e) {
		e.next().hide();
		e.css("border", "2px none red");
	}
	
	
	
	
	
	$(".content form").submit(function() {
		error = false;
		
		// firstname validation
		// firstname != ""
		if ($("input[name=prenom]").val() == "") {
			invalid($("input[name=prenom]"), "The firstname is mandatory.");
		} else {
			valid($("input[name=prenom]"));
		}
		
		// lastname validation
		// lastname != ""
		if ($("input[name=nom]").val() == "") {
			invalid($("input[name=nom]"), "The lastname is mandatory.");
		} else {
			valid($("input[name=nom]"));
		}
		return !error;
	});
	
});