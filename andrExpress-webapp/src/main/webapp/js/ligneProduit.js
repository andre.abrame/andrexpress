
$(document).ready(function(){
	

	$(".Garbage").click(function() {
		$(this).parent().parent().remove();
	});

	
	
	
	function updatePanier(productId, oQ, nQ) {
		$.ajax({
			  method: "POST",
			  url: "panier",
			  data: {product: productId,quantity: nQ }
			})
			.done(function() {
				if (nQ > 0) {
					console.log("updating ligne");
					// updating quantity
					$(".QteProduit input[data-id="+productId+"]").val(nQ);
					$(".QteProduit input[data-id="+productId+"]").attr("data-previous-value", nQ);
					// updating ligneProduit
					let pU = Number($(".prixUnitaire[data-id="+productId+"]").text());
					$(".prixTotal[data-id="+productId+"]").text(nQ*pU);
					// updating panier total
					let t = Number($(".prixTotalPanierValue").text());
					$(".prixTotalPanierValue").text(t - (oQ*pU) + (nQ*pU))
				} else {
					console.log("removin ligne");
					// updating panier total
					let pU = Number($(".prixUnitaire[data-id="+productId+"]").text());
					let t = Number($(".prixTotalPanierValue").text());
					$(".prixTotalPanierValue").text(t - (oQ*pU))
					// removing ligneProduit
					$(".ligneProduit[data-id="+productId+"]").remove();
				}
			});
	};


	$(".QteProduit .ligneProduit-plus").click(function() {
		let oQ = Number($(this).next().val());
		let nQ = oQ+1;
		let id = $(this).attr("data-id");
		updatePanier(id, oQ, nQ);
	});

	$(".QteProduit .ligneProduit-moins").click(function() {
		let oQ = Number($(this).prev().val());
		let nQ = oQ-1;
		let id = $(this).attr("data-id");
		updatePanier(id, oQ, nQ);
	});    



	$(".QteProduit input").on("change", function(){
		let oQ = $(this).attr("data-previous-value");
		let nQ = $(this).val();
		let id = Number($(this).attr("data-id"));
		updatePanier(id, oQ, nQ);
	});

	$(".ligneProduit .ligneProduit-supprime").click(function() {
		console.log("supprime")
		let id = $(this).attr("data-id");
		let oQ = Number($(".QteProduit input[data-id="+id+"]").val());
		let nQ = 0;
		updatePanier(id, oQ, nQ);
	});    


});