package andrExpress.controllers;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andrExpress.models.Client;
import andrExpress.models.Commentaire;
import andrExpress.models.Produit;
import andrExpress.services.ProduitService;


@WebServlet("/product/comments")
public class CommentsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	ProduitService ps = new ProduitService();

	public CommentsServlet() {
		super();

	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		Produit p = ps.findProductById(id);
		request.setAttribute("product", p);
		request.getRequestDispatcher(request.getContextPath() + "../../WEB-INF/jsp/productComments.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("---------------------" +request.getContextPath());
		boolean error = false;		
		Client client = (Client) request.getSession().getAttribute("user");
		int id = Integer.parseInt(request.getParameter("id"));
		Produit p = ps.findProductById(id);
		
		Commentaire com = new Commentaire(    
				Integer.parseInt(request.getParameter("note")), 
				request.getParameter("texte"), 
				request.getParameter("titre"), 
				LocalDateTime.now(),
				p, 
				client);
		System.out.println(request.getParameter("texte"));
		

		try {
			ps.ajouterCommentaire(p, client, com);
		} catch (Exception e) {
			error = true;
			throw e;
		}

		// navigation
		if (error) {
			request.setAttribute("editedComment", com);
			request.getRequestDispatcher(request.getContextPath() + "../../WEB-INF/jsp/productComments.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher(request.getContextPath() + "../../WEB-INF/jsp/productComments.jsp").forward(request, response);
		}
	}

}
