package andrExpress.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.EmailValidator;

import andrExpress.models.Adresse;
import andrExpress.models.Client;
import andrExpress.services.UtilisateurService;

@WebServlet("/client/new")
public class ClientNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService us = new UtilisateurService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../WEB-INF/jsp/clientNew.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		// construction du client saisi (nouveau)
		Client newC = new Client(
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("email"),
				request.getParameter("username"),
				request.getParameter("password"),
				0);
		// création des adresses et liaisons avec le client
		String[] ids = request.getParameterValues("id");
		String[] rues = request.getParameterValues("rue");
		String[] villes = request.getParameterValues("ville");
		String[] codepostaux = request.getParameterValues("codepostal");
		if (rues != null) {
			for (int i=0; i<rues.length; ++i) {
				// creation
				Adresse a = new Adresse(rues[i], villes[i], codepostaux[i]);
				// liaison
				newC.getAdresses().add(a);
				a.setPersonne(newC);
			}
		}
		// validation
		if ((newC.getUsername().isEmpty()) ||
			(newC.getPassword().isEmpty()) ||
			(newC.getEmail().isEmpty()) ||
			(!EmailValidator.getInstance().isValid(newC.getEmail())) ||
			(newC.getPrenom().isEmpty()) ||
			(newC.getNom().isEmpty())) {
			request.setAttribute("errorMessage", "Invalid input.");
			error = true;
		}
		// enregistrement
		if (!error) {
			try {
				us.ajouterClient(newC);
			} catch(PersistenceException e) {
				request.setAttribute("errorMessage", "Server error");
				error = true;
			}
		}
		// navigation
		if (error) {
			request.setAttribute("editedUser", newC);
			request.getRequestDispatcher("../WEB-INF/jsp/clientNew.jsp").forward(request, response);
		} else {
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}
