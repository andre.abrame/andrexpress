package andrExpress.controllers;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andrExpress.utils.ConfigurationUtil;

@WebServlet("/img/*")
public class ImageServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    	// recuperation du nom de fichier dans l'URL 
    	String fileName = request.getPathInfo().substring(1);
    	// ouverture d'un InputStream
    	InputStream is = new FileInputStream(new File(ConfigurationUtil.getProperty("images.path") + fileName));
    	// ouverture d'un outputStream
    	OutputStream os = response.getOutputStream();
    	// envoie de l'image
    	is.transferTo(os);
    }
}
