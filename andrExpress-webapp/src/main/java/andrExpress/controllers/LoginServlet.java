package andrExpress.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import andrExpress.models.Client;
import andrExpress.models.LigneProduitsDatee;
import andrExpress.models.Utilisateur;
import andrExpress.services.UtilisateurService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService us = new UtilisateurService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
	}



	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur u = null;
		try {
			u = us.signIn(request.getParameter("username"),
						  request.getParameter("password"));
		} catch(PersistenceException e) {
			request.setAttribute("errorMessage", "Unable to connect to database. Please try again later..");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		}
		if (u == null) {
			request.setAttribute("errorMessage", "Username or password invalid.");
			request.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(request, response);
		} else {
			HttpSession session = request.getSession();
			session.setAttribute("user", u);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}
