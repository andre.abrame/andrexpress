package andrExpress.controllers;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andrExpress.models.Adresse;
import andrExpress.models.Client;
import andrExpress.models.Utilisateur;
import andrExpress.services.UtilisateurService;

@WebServlet("/client/edit")
public class ClientEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService us = new UtilisateurService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Utilisateur u = (Utilisateur) request.getSession().getAttribute("user");
		request.setAttribute("editedUser", u);
		request.getRequestDispatcher("../WEB-INF/jsp/clientEdit.jsp").forward(request, response);
	}

	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		// recuperation du client connecté (ancien)
		Utilisateur oldC = (Utilisateur) request.getSession().getAttribute("user");
		// construction du client saisi (nouveau)
		Client newC = new Client(
				oldC.getId(),
				request.getParameter("nom"),
				request.getParameter("prenom"),
				request.getParameter("email"),
				oldC.getUsername(),
				(request.getParameter("newPassword").isEmpty()) ? oldC.getPassword() : request.getParameter("newPassword"),
				0);
		// création des adresses et liaisons avec le client
		String[] ids = request.getParameterValues("id");
		String[] rues = request.getParameterValues("rue");
		String[] villes = request.getParameterValues("ville");
		String[] codepostaux = request.getParameterValues("codepostal");
		if (rues != null) {
			for (int i=0; i<rues.length; ++i) {
				// creation
				Adresse a = new Adresse(rues[i], villes[i], codepostaux[i]);
				// liaison
				newC.getAdresses().add(a);
				a.setPersonne(newC);
			}
		}
		// validation
		if ((!request.getParameter("newPassword").isEmpty()) &&
				(!request.getParameter("oldPassword").equals(oldC.getPassword()))) {
			error = true;
			request.setAttribute("passwordErrorMessage", "Wrong password");
		}
		// mise a jour de la base de donnée
		try {
			us.editerClient(oldC, newC);
		} catch(PersistenceException e) {
			error = true;
			request.setAttribute("usernameError", "Updtaing failed (" + e.getMessage() + ")");
		}
		// navigation
		if (error) {
			request.setAttribute("editedUser", newC);
			request.getRequestDispatcher(request.getContextPath() + "../WEB-INF/jsp/clientEdit.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("user", newC);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
	}

}
