package andrExpress.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andrExpress.models.Client;
import andrExpress.services.UtilisateurService;

@WebServlet("/panier")
public class PanierServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    
    private UtilisateurService us = new UtilisateurService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("WEB-INF/jsp/panier.jsp").forward(request, response);
    }
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int nb = Integer.parseInt(request.getParameter("quantity"));
		int idProduit = Integer.parseInt(request.getParameter("product"));
		Client c = (Client) request.getSession().getAttribute("user");
		us.updatePanier(c, idProduit, nb);
		response.setContentType("text/html; charset=utf-8");
		response.setStatus(HttpServletResponse.SC_OK);
	}

}
