package andrExpress.controllers;

import java.io.IOException;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.validator.routines.EmailValidator;

import andrExpress.models.Adresse;
import andrExpress.models.Client;
import andrExpress.services.UtilisateurService;

@WebServlet("/client/exists")
public class ClientExistsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UtilisateurService us = new UtilisateurService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String un = request.getParameter("username");
		if (us.usernameExists(un))
			response.getWriter().write("true");
		else
			response.getWriter().write("false");
	}



}
