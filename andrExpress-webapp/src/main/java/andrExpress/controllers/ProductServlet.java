package andrExpress.controllers;

import andrExpress.models.Produit;
import andrExpress.services.ProduitService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/product/view")
public class ProductServlet extends HttpServlet {

    ProduitService ps = new ProduitService();

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean error = false;
        int id =0;

        //recuperation de l'id
        try {
            id = Integer.parseInt(request.getParameter("id"));
        }catch (NumberFormatException e){
            error=true;
            request.setAttribute("errorMessage", "Not a number");

        }

        //recuperation du client
        if(!error){
            Produit newProduit = ps.findProductById(id);

            if(newProduit!=null)
            {
                request.setAttribute("product", ps.findProductById(id));
                //Affichage de la page produit pour newProduit
                request.getRequestDispatcher("../WEB-INF/jsp/product.jsp").forward(request, response);
            } else {
                error=true;
            }
        }

        //redirection si erreur
        if(error) {
            response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
        }
    }
}
