package andrExpress.controllers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import andrExpress.models.Produit;
import andrExpress.models.Vendeur;
import andrExpress.services.ImageService;
import andrExpress.services.ProduitService;

@WebServlet("/product/edit")
@MultipartConfig
public class ProductEditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProduitService ps = new ProduitService();
	private ImageService is = new ImageService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("test 1");
		Produit p = ps.findProductById(id);
		request.setAttribute("produit", p);
		System.out.println("jbkjbkjb");
		System.out.println(request.getContextPath());
		request.getRequestDispatcher(request.getContextPath() + "../../WEB-INF/jsp/productEdit.jsp").forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean error = false;
		
		int id = Integer.parseInt(request.getParameter("id"));
				
		Produit oldP = ps.findProductById(id);

		Set<File> images = new HashSet<File>();
		if (request.getParameterValues("filesFromUrl") != null) {
			for (String fileUrl : request.getParameterValues("filesFromUrl")) {
				URL url = new URL(fileUrl);
				InputStream fileContent = url.openStream();
				images.add(is.saveImage(fileContent, request.getParameter("name")));
			}
		}

		for (Part part : request.getParts()) {
			if ((part.getContentType() != null) && 
				part.getContentType().equals("image/jpeg")) {
				InputStream fileContent = part.getInputStream();
				images.add(is.saveImage(fileContent, request.getParameter("name")));
			}
		}

		Produit newP = new Produit(request.getParameter("name"),
				request.getParameter("description"),
				Double.parseDouble(request.getParameter("price")),
				new HashSet<Vendeur>());
		
		
		newP.setImages(images);
		newP.setId(oldP.getId());

		// mise a jour de la base de donnée
		try {
			ps.updateProduct(oldP, newP);
		} catch(PersistenceException e) {
			error = true;
			request.setAttribute("productError", "Updtaing failed (" + e.getMessage() + ")");
		}
		// navigation
		if (error) {
			request.setAttribute("produit", oldP);
			request.getRequestDispatcher(request.getContextPath() + "../WEB-INF/jsp/productEdit.jsp").forward(request, response);
		} else {
		//	request.getSession().setAttribute("produit", newP);
			response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
		}
		
		
	}
}
