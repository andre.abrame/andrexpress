package andrExpress.controllers;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import andrExpress.models.Produit;
import andrExpress.services.ProduitService;

@WebServlet("/search")
public class SearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProduitService ps = new ProduitService();
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String searched = request.getParameter("searched");
		List<Produit> lp = ps.filtrerParTexteEtPrix(searched, 0, Double.MAX_VALUE);
		request.setAttribute("products", lp);
		request.getRequestDispatcher("WEB-INF/jsp/products.jsp").forward(request, response);
	}

	

}
