package andrExpress.controllers;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import andrExpress.models.Produit;
import andrExpress.models.Vendeur;
import andrExpress.services.ImageService;
import andrExpress.services.ProduitService;

@WebServlet("/product/new")
@MultipartConfig
public class ProductNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private ProduitService ps = new ProduitService();
	private ImageService is = new ImageService();


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("../WEB-INF/jsp/productNew.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Set<File> images = new HashSet<File>();
		if (request.getParameterValues("filesFromUrl") != null) {
			for (String fileUrl : request.getParameterValues("filesFromUrl")) {
				URL url = new URL(fileUrl);
				InputStream fileContent = url.openStream();
				images.add(is.saveImage(fileContent, request.getParameter("name")));
			}
		}

		for (Part part : request.getParts()) {
			if ((part.getContentType() != null) && 
				part.getContentType().equals("image/jpeg")) {
				InputStream fileContent = part.getInputStream();
				images.add(is.saveImage(fileContent, request.getParameter("name")));
			}
		}

		Produit p = new Produit(request.getParameter("name"),
								request.getParameter("description"),
								Double.parseDouble(request.getParameter("price")),
								new HashSet<Vendeur>());
		p.setImages(images);
		ps.ajouterProduit(p);

		response.sendRedirect(response.encodeRedirectURL(request.getContextPath() + "/"));
	}

}
