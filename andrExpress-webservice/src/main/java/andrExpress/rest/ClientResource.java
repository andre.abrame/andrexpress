package andrExpress.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import andrExpress.models.Client;
import andrExpress.services.UtilisateurService;

@Path("/clients")
@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
public class ClientResource {

	private UtilisateurService us = new UtilisateurService();
	
	@GET
	public Response clientsGet() {
		return Response.ok().entity(us.findAllClient()).build();
	}
	
	@GET
	@Path("{id}")
	public Client clientGet(@PathParam("id") int id) {
		return us.findClientById(id);
	}

	@DELETE
	@Path("{id}")
	public Response clientDelete(@PathParam("id") int id) {
		if (us.deleteClientById(id))
			return Response.ok().build();
		return Response.ok().build();
	}
	
	@POST
	public Response clientPost(Client c) {
		us.ajouterClient(c);
		return Response.ok().build();
	}
	
	@PUT
	@Path("{id}")
	public Response clientPut(@PathParam("id") int id, Client nc) {
		// recuperer l'ancien client dans la base de donnees
		Client oc = us.findClientById(id);
		// si non trouve => on renvoie BAD_REQUEST
		if (oc == null)
			return Response.status(Status.BAD_REQUEST).build();
		// sinon :
		//   - on ajoute l'id au nouveau client
		nc.setId(id);
		//   - on fait l'edition: us.editerClient(...)
		us.editerClient(oc, nc);
		//   - on renvoie OK
		return Response.ok().build();
	}
	
}
