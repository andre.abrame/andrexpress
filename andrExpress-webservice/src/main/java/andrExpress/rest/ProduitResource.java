package andrExpress.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import andrExpress.models.Produit;
import andrExpress.services.ProduitService;

@Path("/products")
@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
public class ProduitResource {

	private ProduitService ps = new ProduitService();
	
	@GET
	public Response produitsGet() {
		return Response.ok().entity(ps.findProductsAll()).build();
	}
	
	@GET
	@Path("{id}")
	public Produit ProduitGet(@PathParam("id") int id) {
		return ps.findProductById(id);
	}

	@DELETE
	@Path("{id}")
	@RolesAllowed({"Admin"})
	public Response ProduitDelete(@PathParam("id") int id) {
		ps.deleteProductById(id);
		return Response.ok().build();
	}
	
	@POST
	@RolesAllowed({"Admin"})
	public Response ProduitPost(Produit c) {
		ps.saveProduct(c);
		return Response.ok().build();
	}
	
	@PUT
	@Path("{id}")
	@RolesAllowed({"Admin"})
	public Response ProduitPut(@PathParam("id") int id, Produit nc) {
		// recuperer l'ancien Produit dans la base de donnees
		Produit oc = ps.findProductById(id);
		// si non trouve => on renvoie BAD_REQUEST
		if (oc == null)
			return Response.status(Status.BAD_REQUEST).build();
		// sinon :
		//   - on ajoute l'id au nouveau Produit
		nc.setId(id);
		//   - on fait l'edition: us.editerProduit(...)
		ps.updateProduct(oc, nc);
		//   - on renvoie OK
		return Response.ok().build();
	}
	
	
	@OPTIONS
	public Response ProduitOption() {
		return Response.ok().build();
	}
}
