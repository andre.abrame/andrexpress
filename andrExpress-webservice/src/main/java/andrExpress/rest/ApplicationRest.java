package andrExpress.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

@ApplicationPath("/ws-rest")
public class ApplicationRest extends ResourceConfig {
	
	public ApplicationRest() {
		packages("andrExpress.rest");
		register(AuthenticationFilter.class);
		register(RolesAllowedDynamicFeature.class);
	}
	
	
}