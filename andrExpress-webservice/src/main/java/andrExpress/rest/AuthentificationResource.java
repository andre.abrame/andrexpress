package andrExpress.rest;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import andrExpress.services.UtilisateurService;

@Path("/authentication")
@Produces({ "application/json", "application/xml" })
@Consumes({ "application/json", "application/xml" })
public class AuthentificationResource {

	private UtilisateurService us = new UtilisateurService();
	
	@GET
	@RolesAllowed({"Admin", "Client"})
	public Response userGet(@Context SecurityContext securityContext) {
		return Response.ok().entity(us.findByUsername(securityContext.getUserPrincipal().getName())).build(); 
	}
	
	
}
