package andrExpress.rest;

import java.io.IOException;

import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.glassfish.jersey.internal.util.Base64;

import andrExpress.models.Utilisateur;
import andrExpress.security.BasicSecurityContext;
import andrExpress.services.UtilisateurService;

@Provider
@Priority(Priorities.AUTHENTICATION)
class AuthenticationFilter implements ContainerRequestFilter {
	private UtilisateurService us = new UtilisateurService();

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		String authzHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
		if (authzHeader != null) {
			String decoded = Base64.decodeAsString(authzHeader.substring(6));
			System.out.println("Authorization : " + decoded);
			String[] split = decoded.split(":");
			Utilisateur user = null;
			if (split.length == 2)
				user = us.signIn(split[0], split[1]);
			if (user != null) {
				SecurityContext oldContext = requestContext.getSecurityContext();
				requestContext.setSecurityContext(new BasicSecurityContext(user, oldContext.isSecure()));
			}
			System.out.println("user : " + user);
		}
	}
}