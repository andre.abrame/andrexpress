package andrExpress.security;

import java.security.Principal;

import javax.ws.rs.core.SecurityContext;

import andrExpress.models.Utilisateur;

public class BasicSecurityContext implements SecurityContext {
	   private final Utilisateur user;
	   private final boolean secure;

	   public BasicSecurityContext(Utilisateur user, boolean secure) {
	       this.user = user;
	       this.secure = secure;
	   }

	   @Override
	   public Principal getUserPrincipal() {
	       return new Principal() {
	           @Override
	           public String getName() {
	                return user.getUsername();
	           }
	       };
	   }

	   @Override
	   public String getAuthenticationScheme() {
	       return SecurityContext.BASIC_AUTH;
	   }

	   @Override
	   public boolean isSecure() { return secure; }

	   @Override
	   public boolean isUserInRole(String role) {
		   System.out.println("isUserInRole(" + role + ") = " + user.getAuthorisations().contains(role));
	       return user.getAuthorisations().contains(role);
	   }
	}