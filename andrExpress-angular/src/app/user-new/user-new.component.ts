import { Component, OnInit } from '@angular/core';
import { User } from '../classes/User';
import { UserService } from '../user.service';

@Component({
	selector: 'app-user-new',
	templateUrl: './user-new.component.html',
	styleUrls: ['./user-new.component.css']
})
export class UserNewComponent implements OnInit {

	user: User = new User();
	// {
	// 	id: undefined,
	// 	nom: "",
	// 	prenom: "",
	// 	email: "",
	// 	adresses: [
	// 		{id: 23, rue: "Cannebiere", ville: "Marseille", codePostal: "13001"},
	// 		{id: 24, rue: "Palais des papes", ville: "Avignon", codePostal: "inconnu"}
	// 	],
	// 	username: "",
	// 	password: "",
	// 	authorizations: []
	// };

	constructor(private us: UserService) { }

	ngOnInit() {
	}

	addUser(u: User) {
		this.us.save(u);
	}
}
