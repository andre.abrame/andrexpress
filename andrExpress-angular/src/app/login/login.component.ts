import { Component, OnInit } from '@angular/core';
import {AuthenticationService } from '../authentication.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	username: string;
	password: string;


	constructor(private as: AuthenticationService) { }

	ngOnInit() {}

	login() {
		console.log("login (" + this.username + ", " + this.password + ") !");
		this.as.login(this.username, this.password);
	}

	reset() {
		this.username = this.username.split("").reverse().join("");
	}

}
