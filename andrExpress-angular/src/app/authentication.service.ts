import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './classes/User';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

	url: string = "http://localhost:8080/andrExpress-webservice/ws-rest/authentication";
	private user: User = undefined;

  constructor(private http: HttpClient) { }


  public login(username: string, password: string) {
    let encoded: string = 'Basic '+btoa(username+':'+password);
    const httpOptions = {
      headers: new HttpHeaders({
        'Authorization': encoded
      })
    };

    this.http.get<User>(this.url, httpOptions).subscribe(res => {
      this.user = res;
    })
  }

  public logout() {
  	this.user = null;
  }


  public getUser(): User {
  	return this.user;
  }

  public hasRole(role: string): boolean {
  	return (this.user == undefined) ? false : this.user.authorisations.includes(role);
  }

}