import { Component, OnInit } from '@angular/core';
import { User } from '../classes/User';
import { UserService } from '../user.service';


@Component({
	selector: 'app-users',
	templateUrl: './users.component.html',
	styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

	private users : User[];

	constructor(private us: UserService) {}

	ngOnInit() {
		this.us.findAll().subscribe(res => {
			this.users = res;
			});
	}

}
