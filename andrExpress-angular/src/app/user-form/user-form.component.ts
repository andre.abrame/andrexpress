import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../classes/User';

@Component({
	selector: 'app-user-form',
	templateUrl: './user-form.component.html',
	styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

	@Input()
	user: User;

	@Output()
	message = new EventEmitter<User>();


	constructor() { }

	ngOnInit() {
	}

	submitForm() {
		this.message.emit(this.user);
	}


}
