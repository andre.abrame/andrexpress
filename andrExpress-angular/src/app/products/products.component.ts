import { Component, OnInit } from '@angular/core';
import { Product } from '../classes/Product';
import { ActivatedRoute } from '@angular/router';
import { ProductService }  from '../product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

	products : Product[];

	view: string = "grid";

  constructor(private router: ActivatedRoute, private ps: ProductService) { }

  ngOnInit() {
  	this.router.paramMap.subscribe(res => {
  		this.view = res.get('view');
  		console.log(this.view);
  	});

    this.ps.findAll().subscribe(res => {
      this.products = res;
    });

  }

}
