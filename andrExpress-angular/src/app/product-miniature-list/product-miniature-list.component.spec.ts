import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMiniatureListComponent } from './product-miniature-list.component';

describe('ProductMiniatureListComponent', () => {
  let component: ProductMiniatureListComponent;
  let fixture: ComponentFixture<ProductMiniatureListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductMiniatureListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMiniatureListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
