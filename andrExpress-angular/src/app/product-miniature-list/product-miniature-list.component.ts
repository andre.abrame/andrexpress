import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../classes/Product';
import { ImageService } from '../image.service';
import { ProductService } from '../product.service';

@Component({
	selector: 'app-product-miniature-list',
	templateUrl: './product-miniature-list.component.html',
	styleUrls: ['./product-miniature-list.component.css']
})
export class ProductMiniatureListComponent implements OnInit {

	@Input()
	product: Product;

	constructor(private is: ImageService, private ps: ProductService) { }

	ngOnInit() {
	}

	public deleteProduct(p) {
		this.ps.delete(p);
	}

}
