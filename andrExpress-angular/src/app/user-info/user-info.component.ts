import { Component, OnInit } from '@angular/core';
import { User } from '../classes/User';


@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class UserInfoComponent implements OnInit {

	user: User = {
		id: 12,
		nom: "Abrame",
		prenom: "André",
		email: "andre.abrame@formation.net",
		adresses: [
			{id: 23, rue: "Cannebiere", ville: "Marseille", codePostal: "13001"},
			{id: 24, rue: "Palais des papes", ville: "Avignon", codePostal: "inconnu"}
		],
		username: "andre",
		password: "andre",
		authorizations: []
	};


  constructor() { }

  ngOnInit() {
  }

}
