import { Component, OnInit } from '@angular/core';
import { Product } from '../classes/Product';
import { ProductService }  from '../product.service';

@Component({
  selector: 'app-product-new',
  templateUrl: './product-new.component.html',
  styleUrls: ['./product-new.component.css']
})
export class ProductNewComponent implements OnInit {

	product: Product = new Product();
 //  {
	// 	id: undefined,
	// 	nom : "",
	// 	description: "",c
	// 	prix: undefined,
	// 	images: ["img/spaghetti.jpg"]
	// }



  constructor(private ps: ProductService) { }

  ngOnInit() {
  }


  saveProduct() {
  	console.log("saving product {" + this.product.id + ", " + this.product.nom + ", " + this.product.description + ", " + this.product.prix + "}");
  	this.ps.save(this.product);
  }

}
