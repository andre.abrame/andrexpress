export class Product {
	id: Number;
	nom: String;
	description: String;
	prix: Number;
	images: String[];
}