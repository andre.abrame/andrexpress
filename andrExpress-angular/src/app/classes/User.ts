import {Address} from './Address';

export class User {
	id: Number;
	nom: String;
	prenom: String;
	email: String;
	adresses: Address[];
	username: String;
	password: String;
	authorisations: String[];
}