
export class Address {
	id: Number;
	rue: String;
	ville: String;
	codePostal: String;
}