import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './classes/User';

@Injectable({
	providedIn: 'root'
})
export class UserService {



	url:string = "http://localhost:8080/andrExpress-webservice/ws-rest/clients";

	constructor(private http: HttpClient) { }


	findAll() {
		return this.http.get<Array<User>>(this.url);
	}

	save(u: User) {
		return this.http.post(this.url, u).subscribe();
	}


	findById(id: number) {
		return this.http.get<User>(this.url + "/"+id);
	}

	update(u: User) {
		return this.http.put(this.url, u).subscribe();
	}

	delete(u: User) {
		console.log("stll deleting");
		return this.http.request('delete', this.url+"/"+u.id).subscribe();
	}




}
