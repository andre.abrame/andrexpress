import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from './classes/Product';

@Injectable({
	providedIn: 'root'
})
export class ProductService {
	url:string = "http://localhost:8080/andrExpress-webservice/ws-rest/products";

	constructor(private http: HttpClient) { }


	findAll() {
		return this.http.get<Array<Product>>(this.url);
	}

	save(p: Product) {
		return this.http.post(this.url, p).subscribe();
	}


	findById(id: number) {
		return this.http.get<Product>(this.url + "/"+id);
	}

	update(p: Product) {
		return this.http.put(this.url, p).subscribe();
	}

	delete(p: Product) {
		return this.http.request('delete', this.url+"/"+p.id).subscribe();
	}
}
