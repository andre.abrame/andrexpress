import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MenuComponent } from './menu/menu.component';
import { ProductsComponent } from './products/products.component';
import { ProductNewComponent } from './product-new/product-new.component';
import { LoginComponent } from './login/login.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { PricePipe } from './price.pipe';
import { UsersComponent } from './users/users.component';
import { UserNewComponent } from './user-new/user-new.component';
import { ProductMiniatureGridComponent } from './product-miniature-grid/product-miniature-grid.component';
import { ProductMiniatureListComponent } from './product-miniature-list/product-miniature-list.component';
import { UserFormComponent } from './user-form/user-form.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { ProductFormComponent } from './product-form/product-form.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { BasicAuthInterceptor } from './basic-auth.interceptor';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent,
    ProductsComponent,
    ProductNewComponent,
    LoginComponent,
    UserInfoComponent,
    PricePipe,
    UsersComponent,
    UserNewComponent,
    ProductMiniatureGridComponent,
    ProductMiniatureListComponent,
    UserFormComponent,
    UserEditComponent,
    ProductFormComponent,
    ProductEditComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
