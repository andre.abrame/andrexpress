import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../classes/Product';
import { ProductService }  from '../product.service';

@Component({
	selector: 'app-product-edit',
	templateUrl: './product-edit.component.html',
	styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {

	product: Product

	constructor(private ar: ActivatedRoute, private ps: ProductService) { }

	ngOnInit() {
		this.ar.paramMap.subscribe(res => { 
			let id = Number(res.get('id')); 
			this.ps.findById(id).subscribe(res => {
				this.product = res;
			})
			
		});
	}


	updateProduct() {
		console.log("saving product {" + this.product.id + ", " + this.product.nom + ", " + this.product.description + ", " + this.product.prix + "}");
		this.ps.update(this.product);
	}

}
