import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProductsComponent } from './products/products.component';
import { ProductNewComponent } from './product-new/product-new.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { UsersComponent } from './users/users.component';
import { UserNewComponent } from './user-new/user-new.component';
import { UserEditComponent } from './user-edit/user-edit.component';


const routes: Routes = [
	{path: 'login', component: LoginComponent},
	{path: 'product/new', component: ProductNewComponent},
	{path: 'product/edit/:id', component: ProductEditComponent},
	{path: 'products/:view', component: ProductsComponent},
	{path: '', redirectTo: 'products/', pathMatch: 'full'},
	{path: 'user/info', component: UserInfoComponent},
	{path: 'users', component: UsersComponent},
	{path: 'user/new', component: UserNewComponent},
	{path: 'user/edit/:id', component: UserEditComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
