import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductMiniatureGridComponent } from './product-miniature-grid.component';

describe('ProductMiniatureGridComponent', () => {
  let component: ProductMiniatureGridComponent;
  let fixture: ComponentFixture<ProductMiniatureGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductMiniatureGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductMiniatureGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
