import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../classes/Product';
import { ImageService } from '../image.service';
import { ProductService } from '../product.service';
import { AuthenticationService } from '../authentication.service';

@Component({
	selector: 'app-product-miniature-grid',
	templateUrl: './product-miniature-grid.component.html',
	styleUrls: ['./product-miniature-grid.component.css']
})
export class ProductMiniatureGridComponent implements OnInit {

	@Input()
	product: Product;

	constructor(private is: ImageService, private ps: ProductService, private as: AuthenticationService) { }

	ngOnInit() {
	}

}
