import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from '../classes/User';
import { UserService } from '../user.service';

@Component({
	selector: 'app-user-edit',
	templateUrl: './user-edit.component.html',
	styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {

	id: number;


	user: User;

	constructor(private ar: ActivatedRoute, private us: UserService) { }

	ngOnInit() {
		this.ar.paramMap.subscribe(res => { 
			this.id = Number(res.get('id')); 
			this.us.findById(this.id).subscribe(res => {
				this.user = res;
			});
		});
	}

	updateUser(u: User) {
		this.us.update(u);
	}

}
