import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../classes/Product';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

	@Input()
	product: Product;


	@Output()
	message = new EventEmitter<Product>();


  constructor() { }

  ngOnInit() {
  }

	onSubmit() {
		this.message.emit(this.product);
	}
}
