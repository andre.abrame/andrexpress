import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'price'
})
export class PricePipe implements PipeTransform {

  transform(value: any, devise?: any): any {
  	// if (value % 1 != 0) {
   //  	return value.toFixed(2) + " €";
  	// } else {
  	// 	return value + " €";
  	// }
  	if (!devise) devise = "€";
  	return ((value % 1 != 0) ? value.toFixed(2) : value) + " " + devise;
  }

}
