import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class ImageService {

	url: string = "http://localhost:8080/andrExpress-webapp/img/";

	constructor() { }


	public getImage(s: string) {
		s = s.substring(s.lastIndexOf("/"));
		return this.url + s;
	}

}
