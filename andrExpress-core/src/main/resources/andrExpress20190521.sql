-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: localhost    Database: andrExpress
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Produit`
--

LOCK TABLES `Produit` WRITE;
/*!40000 ALTER TABLE `Produit` DISABLE KEYS */;
INSERT INTO `Produit` VALUES (1,'Imprimer vos diagrammes avec cette super imprimante !','Imprimante',123),(2,'pour faire de l\'UML','Ordinateur',2000),(3,'pour voir l\'UML','Ecran',300),(6,'pour aller enseigner l\'UML','Train',250000),(7,'pour aller encore plus vite pour enseigner encore plus d\'UML','Velo',200),(8,'si jamais vous ratez votre train','Voiture',10000),(11,'pour s\'hydrater en enseigant l\'UML','bouilloire',30),(12,'pour éviter l\'hypoglicémie en enseignant ...','thé sucré',5);
/*!40000 ALTER TABLE `Produit` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Dumping data for table `Adresse`
--

LOCK TABLES `Adresse` WRITE;
/*!40000 ALTER TABLE `Adresse` DISABLE KEYS */;
/*!40000 ALTER TABLE `Adresse` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Commande`
--

LOCK TABLES `Commande` WRITE;
/*!40000 ALTER TABLE `Commande` DISABLE KEYS */;
/*!40000 ALTER TABLE `Commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Commande_LigneProduits`
--

LOCK TABLES `Commande_LigneProduits` WRITE;
/*!40000 ALTER TABLE `Commande_LigneProduits` DISABLE KEYS */;
/*!40000 ALTER TABLE `Commande_LigneProduits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Commentaire`
--

LOCK TABLES `Commentaire` WRITE;
/*!40000 ALTER TABLE `Commentaire` DISABLE KEYS */;
INSERT INTO `Commentaire` VALUES (4,5,'Super de pouvoir imprimer mes diagrammes §',NULL,1),(5,5,'Trs bonne imprimante',NULL,1),(9,5,'Train confortable, mais pas assez rapide.',NULL,6),(10,5,'Manque un espace pour les vélos',NULL,6),(14,5,'J\'aime le thé',NULL,12),(15,5,'Et le thé m\'aime aussi',NULL,12);
/*!40000 ALTER TABLE `Commentaire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `InformationPaiement`
--

LOCK TABLES `InformationPaiement` WRITE;
/*!40000 ALTER TABLE `InformationPaiement` DISABLE KEYS */;
/*!40000 ALTER TABLE `InformationPaiement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `LigneProduits`
--

LOCK TABLES `LigneProduits` WRITE;
/*!40000 ALTER TABLE `LigneProduits` DISABLE KEYS */;
/*!40000 ALTER TABLE `LigneProduits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `LigneProduitsDatee`
--

LOCK TABLES `LigneProduitsDatee` WRITE;
/*!40000 ALTER TABLE `LigneProduitsDatee` DISABLE KEYS */;
/*!40000 ALTER TABLE `LigneProduitsDatee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Panier`
--

LOCK TABLES `Panier` WRITE;
/*!40000 ALTER TABLE `Panier` DISABLE KEYS */;
/*!40000 ALTER TABLE `Panier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Panier_LigneProduitsDatee`
--

LOCK TABLES `Panier_LigneProduitsDatee` WRITE;
/*!40000 ALTER TABLE `Panier_LigneProduitsDatee` DISABLE KEYS */;
/*!40000 ALTER TABLE `Panier_LigneProduitsDatee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Personne`
--

LOCK TABLES `Personne` WRITE;
/*!40000 ALTER TABLE `Personne` DISABLE KEYS */;
/*!40000 ALTER TABLE `Personne` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Personne_Produit`
--

LOCK TABLES `Personne_Produit` WRITE;
/*!40000 ALTER TABLE `Personne_Produit` DISABLE KEYS */;
/*!40000 ALTER TABLE `Personne_Produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Produit_Produit`
--

LOCK TABLES `Produit_Produit` WRITE;
/*!40000 ALTER TABLE `Produit_Produit` DISABLE KEYS */;
/*!40000 ALTER TABLE `Produit_Produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Produit_images`
--

LOCK TABLES `Produit_images` WRITE;
/*!40000 ALTER TABLE `Produit_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `Produit_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16),(16);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

ALTER SEQUENCE `hibernate_sequence` RESTART WITH 16;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-05-21  7:25:57
