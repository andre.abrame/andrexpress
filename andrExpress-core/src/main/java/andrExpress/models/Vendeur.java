package andrExpress.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;

@Entity
public class Vendeur extends Utilisateur {

	@ManyToMany
	private Set<Produit> produits = new HashSet<Produit>();
	
}
