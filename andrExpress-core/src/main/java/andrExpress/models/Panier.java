package andrExpress.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Panier {

	@Id
	@GeneratedValue
	private int id;
	@OneToOne(mappedBy="panier")
	private Client client;
	@OneToMany(fetch=FetchType.EAGER)
	private Set<LigneProduitsDatee> produits = new HashSet<LigneProduitsDatee>();
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Set<LigneProduitsDatee> getProduits() {
		return produits;
	}
	public void setProduits(Set<LigneProduitsDatee> produits) {
		this.produits = produits;
	}
	public int getNbProduits() {
		int nb =0;
		for (LigneProduitsDatee lpd : this.produits) {
			nb += lpd.getNb();
		}
		return nb;
	}	
	public double total() {
		double total = 0;
		for (LigneProduitsDatee lpd : this.produits) 
			total += lpd.total();
		return total;
	}
	
	public int getQuantite(Produit p) {
		for (LigneProduitsDatee lpd : produits) {
			if (lpd.getProduit().getId() == p.getId())
				return lpd.getNb();
		}
		return 0;
	}
	
}
