package andrExpress.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class InformationPaiement {

	@Id
	@GeneratedValue
	private int id;
	private int numeroCb;
	private int code;
	private String nom;
	@ManyToOne
	private Client client;
	
	
}
