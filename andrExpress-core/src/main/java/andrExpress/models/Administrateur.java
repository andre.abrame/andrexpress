package andrExpress.models;

import javax.persistence.Entity;

@Entity
public class Administrateur extends Utilisateur {

	private double salaire;

	public Administrateur() {
		super();
		this.salaire = 0;
		this.getAuthorisations().add("Admin");
	}

	public Administrateur(String nom, String prenom, String email, String username, String password, double salaire) {
		super(nom, prenom, email, username, password);
		this.salaire = salaire;
		this.getAuthorisations().add("Admin");
	}
	
	
}
