package andrExpress.models;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Commentaire {

	@Id
	@GeneratedValue
	private int id;
	private int note;
	private String texte;
	private String titre;
	private LocalDateTime date;	
	@ManyToOne(cascade=CascadeType.ALL)
	private Produit produit;
	@ManyToOne
	private Client client;
	
	

	public Commentaire() {
		super();
	}
	



	public Commentaire(int note, String texte, String titre, LocalDateTime date, Produit produit, Client client) {
		super();
		this.note = note;
		this.texte = texte;
		this.titre = titre;
		this.date = date;
		this.produit = produit;
		this.client = client;
	}




	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getNote() {
		return note;
	}


	public void setNote(int note) {
		this.note = note;
	}


	public String getTexte() {
		return texte;
	}


	public void setTexte(String texte) {
		this.texte = texte;
	}


	public String getTitre() {
		return titre;
	}


	public void setTitre(String titre) {
		this.titre = titre;
	}




	public Produit getProduit() {
		return produit;
	}


	public void setProduit(Produit produit) {
		this.produit = produit;
	}


	public Client getClient() {
		return client;
	}


	public void setClient(Client client) {
		this.client = client;
	}

	
	

	public LocalDateTime getDate() {
		return date;
	}




	public void setDate(LocalDateTime date) {
		this.date = date;
	}




	@Override
	public String toString() {
		return "Commentaire [id=" + id + ", note=" + note + ", texte=" + texte + ",titre" + titre + ", date" + date+ ", produit=" + produit + ", client="
				+ client + "]";
	}


	
	
}
