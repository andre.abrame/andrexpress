package andrExpress.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Client extends Utilisateur {

	private int num;
	@OneToOne(cascade=CascadeType.ALL)
	private Panier panier;
	@OneToMany(mappedBy="client", fetch=FetchType.EAGER)
	private Set<InformationPaiement> informationPaiement = new HashSet<InformationPaiement>();
	@OneToMany(mappedBy="client", fetch=FetchType.EAGER)
	private Set<Commande> commandes = new HashSet<Commande>();
	@OneToMany(mappedBy="client", fetch=FetchType.EAGER)
	private Set<Commentaire> commentaires = new HashSet<Commentaire>();
	

	public Client() {
		super();
		this.panier = new Panier();
		this.getAuthorisations().add("Client");
	}
	
	public Client(int id, String nom, String prenom, String email, String username, String password, int num) {
		super(id, nom, prenom, email, username, password);
		this.num = num;
		this.panier = new Panier();
		this.getAuthorisations().add("Client");
	}
	
	public Client(String nom, String prenom, String email, String username, String password, int num) {
		super(nom, prenom, email, username, password);
		this.num = num;
		this.panier = new Panier();
		this.getAuthorisations().add("Client");
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Panier getPanier() {
		return panier;
	}

	public void setPanier(Panier panier) {
		this.panier = panier;
	}

	public Set<InformationPaiement> getInformationPaiement() {
		return informationPaiement;
	}

	public void setInformationPaiement(Set<InformationPaiement> informationPaiement) {
		this.informationPaiement = informationPaiement;
	}

	public Set<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(Set<Commande> commandes) {
		this.commandes = commandes;
	}

	public Set<Commentaire> getCommentaires() {
		return commentaires;
	}

	public void setCommentaires(Set<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}

	@Override
	public String toString() {
		return "Client [getUsername()=" + getUsername() + ", getPassword()=" + getPassword() + ", getId()=" + getId()
				+ ", getNom()=" + getNom() + ", getPrenom()=" + getPrenom() + ", getEmail()=" + getEmail() + "]";
	}  


	
	
	
}
