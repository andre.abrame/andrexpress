package andrExpress.models;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Produit {

	@Id
	@GeneratedValue
	private int id;
	private String nom;
	private String description;
	private double prix;
	
	@ElementCollection(fetch=FetchType.EAGER)
	private Set<File> images = new HashSet<File>();
	
	@OneToMany(fetch=FetchType.EAGER)
	private Set<Produit> similaires = new HashSet<Produit>();
	
	@OneToMany(mappedBy="produit", fetch=FetchType.EAGER, cascade= {CascadeType.REMOVE, CascadeType.PERSIST})
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	@JsonIdentityReference(alwaysAsId=true)
	private List<Commentaire> commentaires = new ArrayList<Commentaire>();
	
	@ManyToMany(mappedBy="produits", fetch=FetchType.EAGER)
	private Set<Vendeur> vendeurs = new HashSet<Vendeur>();
	
	@OneToMany(mappedBy="produit", fetch=FetchType.EAGER)
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
	@JsonIdentityReference(alwaysAsId=true)
	private Set<LigneProduits> lignesProduits = new HashSet<LigneProduits>();
	
	
	public Produit() {
		super();
	}

	public Produit(String nom, String description, double prix, Set<Vendeur> vendeurs) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		this.vendeurs = vendeurs;
	}

	
	public double averageRating() {
		double sr = 0;
		for (Commentaire c : commentaires)
			sr += c.getNote();
		return sr/commentaires.size();
	}
	
	public int nbRatings() {
		return commentaires.size();
	}
	

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public double getPrix() {
		return prix;
	}


	public void setPrix(double prix) {
		this.prix = prix;
	}


	public Set<File> getImages() {
		return images;
	}


	public void setImages(Set<File> images) {
		this.images = images;
	}


	public Set<Produit> getSimilaires() {
		return similaires;
	}


	public void setSimilaires(Set<Produit> similaires) {
		this.similaires = similaires;
	}


	public List<Commentaire> getCommentaires() {
		return commentaires;
	}


	public void setCommentaires(List<Commentaire> commentaires) {
		this.commentaires = commentaires;
	}


	public Set<Vendeur> getVendeurs() {
		return vendeurs;
	}


	public void setVendeurs(Set<Vendeur> vendeurs) {
		this.vendeurs = vendeurs;
	}


	public Set<LigneProduits> getLignesProduits() {
		return lignesProduits;
	}


	public void setLignesProduits(Set<LigneProduits> lignesProduits) {
		this.lignesProduits = lignesProduits;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", nom=" + nom + ", description=" + description + ", prix=" + prix + "]";
	}
	
	
	
}
