package andrExpress.models;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Commande {

	@Id
	@GeneratedValue
	private int id;
	private ZonedDateTime date;
	private EtatCommande etat;
	@ManyToOne
	private Client client;
	@OneToOne
	private InformationPaiement informationPaiement;
	@OneToMany
	private List<LigneProduits> produits = new ArrayList<LigneProduits>();
	@ManyToOne
	private Adresse adresseFacturation;
	@ManyToOne
	private Adresse adresseLivraison;
	
}
