package andrExpress.models;

public enum EtatCommande {
	LIVREE,
	EN_LIVRAISON,
	EN_PREPARATION,
	ANNULEE
}
