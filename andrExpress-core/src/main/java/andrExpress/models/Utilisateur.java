package andrExpress.models;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;

@Entity
public abstract class Utilisateur extends Personne {

	@Column(unique=true)
	private String username;
	private String password;
	@ElementCollection(fetch = FetchType.EAGER)
	private Set<String> authorisations = new HashSet<String>();
	

	public Utilisateur() {
		super();
	}

	public Utilisateur(int id, String nom, String prenom, String email, String username, String password) {
		super(id, nom, prenom, email);
		this.username = username;
		this.password = password;
	}

	public Utilisateur(String nom, String prenom, String email, String username, String password) {
		super(nom, prenom, email);
		this.username = username;
		this.password = password;
	}
	
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<String> getAuthorisations() {
		return authorisations;
	}

	public void setAuthorisations(Set<String> authorisations) {
		this.authorisations = authorisations;
	}

	public boolean hasRole(String r) {
		return this.authorisations.contains(r);
	}



	
	
}
