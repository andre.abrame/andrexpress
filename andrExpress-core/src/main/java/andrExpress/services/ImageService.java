package andrExpress.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import andrExpress.utils.ConfigurationUtil;

public class ImageService {

	public File saveImage(InputStream fileContent, String name) throws IOException {
		String path = ConfigurationUtil.getProperty("images.path");
		Path destination = Paths.get(path + name +"-0.jpg");
		int i=1;
		while (destination.toFile().exists()) {
			destination = Paths.get(path + name + "-" + i +".jpg");
			i++;
		}
		Files.copy(fileContent, destination, StandardCopyOption.REPLACE_EXISTING);
		return destination.toFile();
	}
	
}
