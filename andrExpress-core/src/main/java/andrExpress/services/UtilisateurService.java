package andrExpress.services;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import andrExpress.daos.AdresseDao;
import andrExpress.daos.ClientDao;
import andrExpress.daos.LigneProduitsDateeDao;
import andrExpress.daos.PanierDao;
import andrExpress.daos.ProduitDao;
import andrExpress.daos.UtilisateurDao;
import andrExpress.models.Adresse;
import andrExpress.models.Client;
import andrExpress.models.LigneProduitsDatee;
import andrExpress.models.Produit;
import andrExpress.models.Utilisateur;
import andrExpress.utils.HibernateUtil;

public class UtilisateurService {

	private ClientDao clientDao;
	private AdresseDao adresseDao = new AdresseDao();
	private UtilisateurDao utilisateurDao = new UtilisateurDao();
	private ProduitDao produitDao = new ProduitDao();
	private LigneProduitsDateeDao ligneProduitsDateeDao = new LigneProduitsDateeDao();
	private PanierDao panierDao = new PanierDao();



	public UtilisateurService() {
		super();
		clientDao = new ClientDao();
	}



	public void ajouterClient(Client c) {
		// enregistrer dans la BD
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			clientDao.save(c, s, t);
			for (Adresse a : c.getAdresses()) {
				a.setPersonne(c);
				adresseDao.save(a, s, t);
			}
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		}
	}

	public boolean usernameExists(String un) {
		return (utilisateurDao.findByUsername(un) != null);
	}


	public Utilisateur signIn(String un, String pwd) {
		return utilisateurDao.findByUsernameAndPassword(un, pwd);
	}


	public void ajouterAdresse(Utilisateur u, String rue, String ville, String codePostal) {

	}



	public void editerClient(Utilisateur ancien, Client nouveau) {
		// construction de la liste des adresses à supprimer
		Set<Adresse> toRemove = new HashSet<Adresse>(ancien.getAdresses());
		toRemove.removeAll(nouveau.getAdresses());
		// enregistrer dans la BD
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			// mise à jour du client
			clientDao.save(nouveau, s, t);
			// sauvegarde / mise à jour des adresses du nouveau client
			for (Adresse a : nouveau.getAdresses()) {
				a.setPersonne(nouveau);
				adresseDao.save(a, s, t);
			}
			// suppression des adresses de l'ancien client
			for (Adresse a : toRemove)
				adresseDao.delete(a, s, t);
			t.commit();
		} catch (HibernateException e) {
			t.rollback();
			throw e;
		}
	}


	public void updatePanier(Client c, int idProduit, int nb) {
		System.out.println("Updating panier");
		// ouverture session & transaction
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			
			LigneProduitsDatee lp = null;
			// recherche du produit (la ligne produit) dans le panier
			for (LigneProduitsDatee lp1 : c.getPanier().getProduits()) {
				if (lp1.getProduit().getId() == idProduit) {
					lp = lp1;
					break;
				}
			}
			if (lp == null) {
				if (nb > 0) {
					System.out.println("Nouvel lp");
					// on recupere le produit
					Produit p = produitDao.findById(idProduit, s);
					// creation de la ligne produit
					lp = new LigneProduitsDatee();
					lp.setNb(nb);
					lp.setProduit(p);
					// liaison de p a lp
					p.getLignesProduits().add(lp);
					// liaison de panier à lp
					c.getPanier().getProduits().add(lp);
					// sauvegarde
//					panierDao.save(c.getPanier(), s, t);
					ligneProduitsDateeDao.save(lp, s, t);
				}
			} else {
				if (nb != 0) {
					System.out.println("MaJ lp");
					// mise a jour de lp
					lp.setNb(nb);
					// sauvegarde
					ligneProduitsDateeDao.save(lp);
				} else {
					System.out.println("Suppression lp");
					// suppression de lp du panier
					System.out.println(c.getPanier().getProduits());
					c.getPanier().getProduits().remove(lp);
					System.out.println(c.getPanier().getProduits());
					// suppression de lp du produit
					System.out.println(lp.getProduit().getLignesProduits());
					lp.getProduit().getLignesProduits().remove(lp);
					System.out.println(lp.getProduit().getLignesProduits());
					lp.setProduit(null);
					panierDao.save(c.getPanier());
					ligneProduitsDateeDao.delete(lp);
				}
			}

			// commit
			t.commit();
		} catch(HibernateException e) {
			t.rollback();
			throw e;
		}
		// fermeture de la session
		s.close();
	}


	public List<Client> findAllClient() {
		return clientDao.findAll();
	}


	public Client findClientById(int id) {
		return clientDao.findById(id);
	}

	public Utilisateur findByUsername(String username) {
		return utilisateurDao.findByUsername(username);
	}


	public boolean deleteClientById(int id) {
		Client c;
		if ((c = clientDao.findById(id)) == null)
			return false;
		clientDao.delete(c);
		return true;
	}

}
