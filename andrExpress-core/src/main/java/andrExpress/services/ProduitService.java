package andrExpress.services;

import java.util.List;

import andrExpress.daos.CommentaireDao;
import andrExpress.models.*;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import andrExpress.daos.CommentaireDao;
import andrExpress.daos.ProduitDao;

import andrExpress.models.Client;
import andrExpress.models.Commentaire;
import andrExpress.models.Produit;
import andrExpress.utils.HibernateUtil;

public class ProduitService {

	private ProduitDao produitDao = new ProduitDao();
	private CommentaireDao commentaireDao = new CommentaireDao();


	

	public List<Produit> findProductsAll() {
		return produitDao.findAll();
	}
	
	public Produit findProductById(int id) {
		return produitDao.findById(id);
	}
	
	public void saveProduct(Produit p) {
		produitDao.save(p);
	}
	
	public void updateProduct(Produit op, Produit np) {
		produitDao.save(np);
	}
	
	public void deleteProductById(int id) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Transaction t = s.beginTransaction();
		try {
			Produit p = produitDao.findById(id, s);
			if (p != null)
				produitDao.delete(p, s, t);
			t.commit();
		} catch(HibernateException e) {
			t.rollback();
			throw e;
		}
	}
	
	public void ajouterProduit(Produit p){
		produitDao.save(p);
	}

	public List<Produit> filtrerParTexteEtPrix(String str, double min, double max) {
		return produitDao.findByNomOuDescriptionEtMinMax(str, min, max);
	}

	public void ajouterCommentaire(Produit p, Client client, Commentaire c){
		p.getCommentaires().add(c);
		c.setClient(client);

		commentaireDao.save(c);
	}

	


}
