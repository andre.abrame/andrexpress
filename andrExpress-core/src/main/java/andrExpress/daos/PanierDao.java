package andrExpress.daos;

import andrExpress.models.Panier;

public class PanierDao extends GenericDao<Panier> {

	public PanierDao() {
		super(Panier.class);
	}
	
}