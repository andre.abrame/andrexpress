package andrExpress.daos;


import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import andrExpress.models.Utilisateur;
import andrExpress.utils.HibernateUtil;

public class UtilisateurDao extends GenericDao<Utilisateur> {

	public UtilisateurDao() {
		super(Utilisateur.class);
	}


	public Utilisateur findByUsername(String username, Session s) {
		return (Utilisateur) s.createQuery("from Utilisateur where username = :username")
				.setParameter("username", username)
				.uniqueResult();
	}

	public Utilisateur findByUsername(String username) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Utilisateur c = findByUsername(username, s);
		s.close();
		return c;
	}
	
	

	public Utilisateur findByUsernameAndPassword(String username, String password, Session s) {
		return (Utilisateur) s.createQuery("from Utilisateur where username = :username and password = :password")
				.setParameter("username", username)
				.setParameter("password", password)
				.uniqueResult();
	}

	public Utilisateur findByUsernameAndPassword(String username, String password) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Utilisateur c = findByUsernameAndPassword(username, password, s);
		s.close();
		return c;
	}
	
	
}
