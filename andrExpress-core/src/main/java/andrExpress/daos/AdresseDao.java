package andrExpress.daos;

import andrExpress.models.Adresse;

public class AdresseDao extends GenericDao<Adresse> {

	public AdresseDao() {
		super(Adresse.class);
	}
	
}