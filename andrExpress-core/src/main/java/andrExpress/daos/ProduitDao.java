package andrExpress.daos;

import java.util.List;

import org.hibernate.Session;

import andrExpress.models.Produit;
import andrExpress.utils.HibernateUtil;


public class ProduitDao extends GenericDao<Produit> {

	public ProduitDao() {
		super(Produit.class);
	}


	public List<Produit> findByNomOuDescriptionEtMinMax(String str, double min, double max, Session s) {
		return s.createQuery("from Produit where (nom like :motif or description like :motif) and prix > :min and prix < :max")
				.setParameter("motif", "%"+str+"%")
				.setParameter("min", min)
				.setParameter("max", max)
				.list();
	}

	public List<Produit> findByNomOuDescriptionEtMinMax(String str, double min, double max) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		List<Produit> lp = findByNomOuDescriptionEtMinMax(str, min, max, s);
		s.close();
		return lp;
	}
	
	
	
}
