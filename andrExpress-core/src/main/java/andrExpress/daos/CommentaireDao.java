package andrExpress.daos;

import andrExpress.models.Commentaire;

public class CommentaireDao extends GenericDao<Commentaire> {

	public CommentaireDao() {
		super(Commentaire.class);
	}
	
}