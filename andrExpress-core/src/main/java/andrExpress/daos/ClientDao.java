package andrExpress.daos;


import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import andrExpress.models.Client;
import andrExpress.utils.HibernateUtil;

public class ClientDao extends GenericDao<Client> {

	public ClientDao() {
		super(Client.class);
	}

	
	
	public Client findByUsername(String username, Session s) {
		return (Client) s.createQuery("from Client where username = :username")
				.setParameter("username", username)
				.uniqueResult();
	}

	public Client findByUsername(String username) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Client c = findByUsername(username, s);
		s.close();
		return c;
	}
	

	public Client findByUsernameAndPassword(String username, String password, Session s) {
//		return (Client) s.createQuery("from Client where username = :username and password = :password")
//				.setParameter("username", username)
//				.setParameter("password", password)
//				.uniqueResult();
		return (Client) s.createCriteria(Client.class)
				.add(Restrictions.and(
						Restrictions.eq("username", username), 
						Restrictions.eq("password",  password)))
				.uniqueResult();
	}

	public Client findByUsernameAndPassword(String username, String password) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Client c = findByUsernameAndPassword(username, password, s);
		s.close();
		return c;
	}
	
	public Client findByIdWithCommentaire(int id) {
		Session s = HibernateUtil.getSessionFactory().openSession();
		Client c = (Client) s.createQuery("from Client c join fetch c.commentaires where c.id = :id")
		.setParameter("id", id)
		.uniqueResult();
		s.close();
		return c;
	}
	
	
}
