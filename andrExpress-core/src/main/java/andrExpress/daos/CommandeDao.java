package andrExpress.daos;

import andrExpress.models.Commande;

public class CommandeDao extends GenericDao<Commande> {

	public CommandeDao() {
		super(Commande.class);
	}
	
}