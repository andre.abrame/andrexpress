package andrExpress.utils;

import java.io.FileInputStream;
import java.net.URLDecoder;
import java.util.Properties;

public class ConfigurationUtil {
    private static final Properties properties;
    
    static {
        try {
        	properties = new Properties();
        	properties.load(new FileInputStream(URLDecoder.decode(ConfigurationUtil.class.getResource("/").getPath(), "UTF-8") + "configuration.properties"));
        } catch (Throwable ex) {
            System.err.println("Configuration loading failed. " + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Properties getProperties() {
        return properties;
    }
    
    public static String getProperty(String name) {
    	return properties.getProperty(name);
    }
}